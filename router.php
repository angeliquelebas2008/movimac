<?php
	require_once('controller.php');

	$page = explode('/',$_SERVER['REQUEST_URI']);
	//var_dump($page);

	$method = $_SERVER['REQUEST_METHOD'];

	switch($page[3]) {
		case 'films' : 
			switch($method) {
				case 'GET' : 
					echo getFilmsAsTable();
					break;
				default:
					http_response_code('404');
					echo 'OOPS';
			}
			break;
		case 'film' :
			switch($method) {
				case 'GET' : 
					echo getAFilm($page[4]);
					break;				
				case 'POST' :
					$json = file_get_contents('php://input');
					echo addAFilm($json);
					break;
	
				case 'DELETE' :
					echo deleteAFilm($page[4]);
					break;
				default:
					http_response_code('404');
					echo 'OOPS';
			}
			break;
		case 'avi' :
			switch($method) {
				case 'GET' : 
					echo getaAvis($page[4],$page[5]);
					break;
				case 'DELETE' :
					echo removeaAvis($page[4],$page[5]);
					break;
				default:
					http_response_code('404');
					echo 'OOPS';
				}
				break;
		case 'avis' : 
			switch($method) {
				case 'GET' : 
					echo getAvisAsTable($page[4]);
					break;
				case 'POST' :
					$json = file_get_contents('php://input');
					echo addaAvis($json);
					break;
				default:
					http_response_code('404');
					echo 'OOPS';
				}
			break;
		case 'playlist' :
			switch($method) {
				case 'GET' : 
					echo getAPlaylist($page[4]);
					break;
				case 'DELETE' :
					echo deleteAPlaylist($page[4]);
					break;
				default:
					http_response_code('404');
					echo 'OOPS';
			}
			break;
		case 'filmsInPlaylist' :
			switch($method) {	
				case 'DELETE' :
					echo deleteAFilmFromPlaylist($page[4] ,$page[5]);
					break;
				case 'GET' : 
					echo getFilmsFromPlaylistAsTable($page[4]);
					break;
				case 'POST' :
					echo addaFilmintoPlaylist($page[4], $page[5]);
					break;
			
				default:
					http_response_code('404');
					echo 'OOPS';
			}
			break;
		case 'playlists' :
			switch($method) {
				case 'GET' : 
					echo getPlaylistsAsTable();
					break;
				case 'POST' :
					$json = file_get_contents('php://input');
					echo addAPlaylist($json);
					break;	
				default:
					http_response_code('404');
					echo 'OOPS';	
			}	
			break;
		case 'hashtag' : 
			switch($method) {
				case 'GET' : 
					echo getAllHashtagsFromFilmAsTable($page[4]);
					break;	
				case 'POST' :
					$json = file_get_contents('php://input');
					echo addAHashtag($json);
					break;
				case 'DELETE' :
					echo deleteAHashtag($page[4], $page[5]);	
					break;	
				default:
					http_response_code('404');
					echo 'OOPS';
			}
			break;
		case 'hashtags' : 
			switch($method) {
				case 'GET' : 
					echo  getallFilmsFromHashtagAsTable($page[4]);
					break;	
				default:
					http_response_code('404');
					echo 'OOPS';
			}
			break;
		case 'recherchefilm' :
			switch($method) {
				case 'GET' : 
					$page[4]=urldecode($page[4]);
					echo recherchesFilms($page[4]);
					break;
		
				default : 
				http_response_code('404');
				echo 'OOPS';	
			}	
			break;
		case 'rechercheplaylist' :
			switch($method) {
				case 'GET' : 
					$page[4]=urldecode($page[4]);
					echo recherchesPlaylists($page[4]);
					break;
				default : 
				http_response_code('404');
				echo 'OOPS';	
			}	
			break;
		case 'upvote' : 
			switch($method) {
				case 'UPDATE' : 
					echo putAnUpVote($page[4],$page[5]);
					break;
				default:
					http_response_code('404');
					echo 'OOPS';
			}
			break;
		case 'downvote' : 
			switch($method) {
				case 'UPDATE' : 
					echo putADownVote($page[4],$page[5]);
					break;
				default:
					http_response_code('404');
					echo 'OOPS';
			}
			break;
		case 'genrefilm' : 
			switch($method) {
				case 'GET' : 
					echo getAllGenresFromFilmAsTable($page[4]);
					break;	
				case 'POST' :
					echo addAGenre($page[4], $page[5]);
					break;
				case 'DELETE' :
					echo deleteAGenre($page[4], $page[5]);	
					break;	
				default:
					http_response_code('404');
					echo 'OOPS';
			}
			break;
		case 'genre' : 
			switch($method) {
				case 'GET' : 
					echo getGenresAsTable();
					break;
				default:
					http_response_code('404');
					echo 'OOPS';
			}	
			break;
		case 'filmgenre' : 
			switch($method) {
				case 'GET' : 
					echo getFilmsFromGenreAsTable($page[4]);
					break;
				default:
					http_response_code('404');
					echo 'OOPS';
			}	
			break;
		default : 
			http_response_code('500');
			echo 'unknown endpoint';
			break;
}