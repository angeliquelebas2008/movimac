<?php
	require "database.php";

	//en ligne
	//$db = new DataBase("angeliquxonfire");

	//en local
	$db = new DataBase("movimac");

	
////////////////////----PLAYLIST----//////////////////////////

	function getPlaylist($titre) {
		$db = new DataBase("movimac");
		$result = $db->prepare('select * from Playlist where idPlaylist=? ', array($titre), false, true);
		return $result;	
	}

	function getAllPlaylists() {
		$db = new DataBase("movimac");
		$result=$db->query('select * from Playlist', false, false);
		return $result;
	}

	function getFilmsFromPlaylist($id) {
		$db = new DataBase("movimac");
		$result = $db->prepare('select Film.idFilm, Film.titre, Film.realisateur, Film.duree, Film.nationalite, Film.annee, Film.lienAffiche, FilmPlaylistRelation.NoteFilm from Film left outer join FilmPlaylistRelation on Film.idFilm = FilmPlaylistRelation.idFilm left outer join Playlist on FilmPlaylistRelation.idPlaylist = Playlist.idPlaylist where Playlist.idPlaylist = ?', array($id), false, false);
		return $result;
	}

	function deleteFilmFromPlaylist($idFilm, $idPlaylist) {
		$db= new DataBase("movimac");
		$result = $db->prepare("delete from FilmPlaylistRelation where idFilm=? and idPlaylist=?",array($idFilm, $idPlaylist), true, false);
		return getFilmsFromPlaylist($idPlaylist);
	}

	function addPlaylist($titre, $description, $auteur, $lienPhoto) {
		$db = new DataBase("movimac");
		$db->prepare('insert into Playlist(titre, description, date, auteur, lienPhoto, MOIS) values( ?, ?, NOW(), ?, ?, ?)',array($titre, $description, $auteur, $lienPhoto, 0), true, false);
		return getAllPlaylists();
	}

	function deletePlaylist($id) {
		$db = new DataBase("movimac");
		$db->prepare("delete from Playlist where idPlaylist=?",array($id), true, false);
		return getAllPlaylists();
	}

////////////////-----------Film---------------/////////////////

	function getAllFilm() {
		$db = new DataBase("movimac");
		$result=$db->query('select * from Film',false,false);
		return $result;
	}

	function getFilm($id) {
		$db = new DataBase("movimac");
		$result = $db->prepare('select * from Film where idFilm=?', array($id), false, true);
		return $result;	
	}
	
	function addFilm($titre, $realisateur, $duree, $nationalite, $annee, $lienAffiche) {
		$db = new DataBase("movimac");
		$db->prepare('insert into Film(titre, realisateur, duree, nationalite, annee, lienAffiche) values(?, ?, ?, ?, ?, ?)', array($titre, $realisateur, $duree, $nationalite, $annee, $lienAffiche), true, false);
		return getAllFilm();
	}

	function deleteFilm($id) {
		$db = new DataBase("movimac");
		$db->prepare("delete from Film where idFilm=?",array($id), true, false);
		return getAllFilm();
	}

	function FilmdansPlaylist($idFilm, $idPlaylist){
        $db = new Database("movimac");
        $db->prepare('insert into FilmPlaylistRelation(idFilm, idPlaylist) values(?, ?)', array($idFilm, $idPlaylist), true, false);
        return getFilmsFromPlaylist($idPlaylist);
    }

////////////////////----Avis----//////////////////////////

	function addAvis($note, $commentaire, $pseudo, $idFilmFK){
		$db = new DataBase("movimac");
		$db->prepare('insert into Avis(note, commentaire, pseudo, idFilmFK) values(?, ?, ?, ?)', array($note, $commentaire, $pseudo, $idFilmFK), true, false);
		return getAllAvis($idFilmFK);
	}

	function getAvis($idFilm,$idAvis){
		$db = new DataBase("movimac");
		$result = $db->prepare('select * from Avis where idAvis=? and idFilmFK=?', array($idAvis,$idFilm),false,true);
		return $result;
	}
	
	function getAllAvis($id) {
		$db = new DataBase("movimac");
		$result = $db->prepare('select * from Avis where idFilmFK=?',array($id),false,false);
		return $result;
	}

	function removeAvis($idAvis, $idFilm) {
		$db = new DataBase("movimac");
		$db->prepare('delete from Avis where idAvis=?', array($idAvis),true,false);
		return getAllAvis($idFilm);
	}

////////////////////----Hashtag----//////////////////////////

	function getAllFilmsFromHashtag($id) {
		$db = new DataBase("movimac");
		$result=$db->prepare("select Film.idFilm, Film.titre, Film.realisateur, Film.duree, Film.nationalite, Film.annee, Film.lienAffiche from Film left outer join FilmHashtagRelation on Film.idFilm = FilmHashtagRelation.idFilm left outer join Hashtag on FilmHashtagRelation.idHashtag = Hashtag.idHashtag where Hashtag.idHashtag =?",array($id),false,false);
		return $result;
	}

	function getAllHashtagsFromFilm($id) {
		$db = new DataBase("movimac");
		$result=$db->prepare("select Hashtag.idHashtag, Hashtag.nom from Hashtag join FilmHashtagRelation on FilmHashtagRelation.idHashtag=Hashtag.idHashtag join Film on FilmHashtagRelation.idFilm=Film.idFilm where Film.idFilm=?",array($id),false,false);
		return $result;
	}

	function addHashtag($idFilm, $nom) {
		$db = new DataBase("movimac");
		$idHashtag=$db->prepare("select idHashtag from Hashtag where nom=?",array($nom),false, true);
		if($idHashtag==NULL){ 
			$db->prepare('insert into Hashtag(nom) values(?)', array($nom), true, false);
			$idHashtag=$db->prepare("select idHashtag from Hashtag where nom=?",array($nom),false,true);
		}
		$idHashtag=$idHashtag->idHashtag;
		$db->prepare('insert into FilmHashtagRelation(idFilm, idHashtag) values(?,?)', array($idFilm, $idHashtag), true, false);
		return getAllHashtagsFromFilm($idFilm);
	}

	function deleteHashtag($idFilm, $idHashtag){
		$db = new DataBase("movimac");
		$db->prepare('delete from FilmHashtagRelation  where idFilm=? and idHashtag=?', array($idFilm,$idHashtag), true, false);
		return getAllHashtagsFromFilm($idFilm);
	}

////////////////////----BARRE DE RECHERCHE----//////////////////////////

	function rechercheFilm($titre){
		$db = new DataBase("movimac");
		$result = $db->prepare('select * from Film where titre like ?', 
								array("%{$titre}%"), false, false);
		return $result;	
	}

	function recherchePlaylist($titre){
		$db = new DataBase("movimac");

		$result = $db->prepare('select * from Playlist where titre like ? ', 
								array("%{$titre}%"), false, false);

		return $result;	
	}

////////////////////----VOTE----//////////////////////////

function addUpVote($idFilm, $idPlaylist){
	$db = new DataBase("movimac");
	$db->prepare('update FilmPlaylistRelation set NoteFilm=NoteFilm+1 where idFilm=? and idPlaylist=?',array($idFilm, $idPlaylist),true,false);
	return getFilmsFromPlaylist($idPlaylist);
}

function addDownVote($idFilm, $idPlaylist){
	$db = new DataBase("movimac");
	$db->prepare('update FilmplaylistRelation set NoteFilm=NoteFilm-1 where idFilm=? and idPlaylist=?',array($idFilm, $idPlaylist),true,false);
	return getFilmsFromPlaylist($idPlaylist);
}

////////////////////----Genre----//////////////////////////

function addGenre($idFilm, $idGenre) {
	$db = new DataBase("movimac");
	$db->prepare('insert into FilmGenreRelation(idFilm, idGenre) values(?, ?)', array($idFilm, $idGenre), true, false);
	return getAllGenresFromFilm($idFilm);
}

function deleteGenre($idFilm, $idGenre){
	$db = new DataBase("movimac");
	$db->prepare('delete from FilmGenreRelation  where idFilm=? and idGenre=?', array($idFilm,$idGenre), true, false);
	return getAllGenresFromFilm($idFilm);
}

function getAllGenresFromFilm($id) {
	$db = new DataBase("movimac");
	$result=$db->prepare("select * from Genre join FilmGenreRelation on FilmGenreRelation.idGenre=Genre.idGenre where idFilm=?",array($id),false,false);
	return $result;
}

function getAllGenres() {
	$db = new DataBase("movimac");
	$result=$db->query("select * from Genre", false, false);
	return $result;
}

function getAllFilmsFromGenre($id) {
	$db = new DataBase("movimac");
	$result=$db->prepare("select Film.idFilm, Film.titre, Film.realisateur, Film.duree, Film.nationalite, Film.annee, 
	Film.lienAffiche from Film left outer join FilmGenreRelation on Film.idFilm = FilmGenreRelation.idFilm left outer join
	 Genre on FilmGenreRelation.idGenre = Genre.idGenre where Genre.idGenre =?",array($id),false,false);
	return $result;
}
