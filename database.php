<?php

class DataBase{

    private $db_name;
    private $db_user;
    private $db_psw;
    private $db_host;
    private $pdo;

    //public function __construct($db_name, $db_user = 'angeliquxonfire', $db_psw = 'ANBMJKKy6aYx34yD4qe7', $db_host ='angeliquxonfire.mysql.db'){
    public function __construct($db_name, $db_user = 'root', $db_psw = '', $db_host ='localhost'){
        $this->db_name = $db_name;
        $this->db_user = $db_user;
        $this->db_psw = $db_psw;
        $this->db_host = $db_host;
    }

    public function getPDO(){
        if($this->pdo === null){
            //$pdo = new PDO('mysql:dbname=angeliquxonfire;host=angeliquxonfire.mysql.db','angeliquxonfire','ANBMJKKy6aYx34yD4qe7');
            $pdo = new PDO('mysql:dbname=movimac;host=localhost','root','');
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo = $pdo;
        }
        return $this->pdo;
    }

    public function query($statement,$insert = false,$one = false){
        $req = $this->getPDO()->query($statement);
        if(!$insert){
            if($one){
                $datas = $req->fetch(PDO::FETCH_OBJ);
            }else{
                $datas = $req->fetchAll(PDO::FETCH_OBJ);
            }
            return $datas;
        }
    }

    public function prepare($statement,$attributes,$insert = false,$one = false){
        $req = $this->getPDO()->prepare($statement);
        $req->execute($attributes);
        $req->setFetchMode(PDO::FETCH_OBJ);
        if(!$insert){
            if($one){
                $datas = $req->fetch();
            }else{
                $datas = $req->fetchAll();
            }
            return $datas;
        }
    }
}

