// document ready in ES6
// here we recreate the famous and wonderful document.ready offered by JQuery !!

Document.prototype.ready = (callback) => {
  if (callback && typeof callback === "function") {
    document.addEventListener("DOMContentLoaded", () => {
      if (
        document.readyState === "interactive" ||
        document.readyState === "complete"
      ) {
        return callback();
      }
    });
  }
};

var idFilmFK;
var monthNames = [
  "Janvier",
  "Février",
  "Mars",
  "Avril",
  "Mai",
  "Juin",
  "Juillet",
  "Août",
  "Septembre",
  "Octobre",
  "Novembre",
  "Decembre",
];
var mois = false;

////////////////////----PLAYLIST----//////////////////////////

//APPEL POUR AFFICHER TOUTES LES PLAYLISTS
document.ready(() => {
  document.getElementById("formFilm").style.display = "none";
  document.getElementById("formAvis").style.display = "none";
  document.getElementById("formPlaylist").style.display = "none";
  document.getElementById("formFilminPlaylist").style.display = "none";
  document.getElementById("formRemplirPlaylist").style.display = "none";
  document.getElementById("listGenre").style.display = "none";
  document.getElementById("listAvis").style.display = "none";
  document.getElementById("formHashtag").style.display = "none";
  fetch("./router.php/playlists")
    .then((response) => response.json())
    .then((data) => {
      getAllPlaylists(data, true);
    })
    .catch((error) => {
      console.log(error);
    });

  fetch("./router.php/films")
    .then((response) => response.json())
    .then((data) => {
      remplirformulairefilm(data);
    })
    .catch((error) => {
      console.log(error);
    });
  fetch("./router.php/genre")
    .then((response) => response.json())
    .then((data) => {
      remplirformulairegenre(data);
    })
    .catch((error) => {
      console.log(error);
    });
});

//REMPLIR LE FORMULAIRE AVEC TOUTES LES PLAYLISTS
function remplirformulaireplaylist(playlists) {
  const formulaire = document.getElementById("playlists_dispo-select");
  var content = "";

  playlists.forEach(function (playlist) {
    content +=
      "<option value=" +
      playlist.idPlaylist +
      ">" +
      playlist.titre +
      "</option>";
  });
  formulaire.innerHTML = content;
}

//FONCTION QUI PERMET DE RECUPERER TOUTES LES PLAYLISTS
function getAllPlaylists(playlists, mois) {
  const partiePlaylist = document.getElementById("listPlaylist");
  partiePlaylist.style.display = "flex";
  document.getElementById("listFilm").style.display = "none";
  document.getElementById("listFilms").style.display = "none";
  document.getElementById("infoPlaylist").style.display = "none";
  document.getElementById("listRecherche").style.display = "none";
  document.getElementById("listHashtag").style.display = "none";
  document.getElementById("formFilminPlaylist").style.display = "none";
  document.getElementById("formRemplirPlaylist").style.display = "none";
  document.getElementById("listGenre").style.display = "none";
  document.getElementById("formGenre").style.display = "none";
  document.getElementById("formAvis").style.display = "none";
  document.getElementById("listAvis").style.display = "none";
  document.getElementById("formHashtag").style.display = "none";
  var content = "";

  playlists.forEach(function (playlist) {
    if (playlist.MOIS == mois) {
      var newDate = new Date(playlist.date);
      content +=
        "<div class='playlist-card'><p>" +
        playlist.titre +
        "</p><p>" +
        monthNames[newDate.getMonth()] +
        " " +
        newDate.getFullYear() +
        "</p><img src = '" +
        playlist.lienPhoto +
        "'/>" +
        "<button class='voirPlaylist-button' onclick='voirPlaylist(\"" +
        playlist.idPlaylist +
        "\")'>Voir Playlist</button></div>";
    }
  });
  remplirformulaireplaylist(playlists);
  partiePlaylist.innerHTML = content;
}

//AFFICHER TOUTES LES PLAYLISTS
function afficherAllPlaylists() {
  fetch("./router.php/playlists", { method: "GET" })
    .then((response) => response.json())
    .then((data) => {
      getAllPlaylists(data, false);
    })
    .catch((error) => {
      console.log(error);
    });
}

//AFFICHER LA PLAYLIST DU MOIS
function afficherPlaylistduMois() {
  fetch("./router.php/playlists", { method: "GET" })
    .then((response) => response.json())
    .then((data) => {
      getAllPlaylists(data, true);
    })
    .catch((error) => {
      console.log(error);
    });
}

//FONCTIONS POUR VOIR UNE SEULE PLAYLIST AVEC LES INFOS ET FILMS QUI CORRESPONDENT
function voirPlaylist(idPlaylist) {
  fetch("./router.php/playlist/" + idPlaylist, {
    method: "GET",
  })
    .then((response) => response.json())
    .then((data) => {
      getInfosFromPlaylist(data);
    })
    .catch((error) => {
      console.log(error);
    });

  fetch("./router.php/filmsInPlaylist/" + idPlaylist, {
    method: "GET",
  })
    .then((response) => response.json())
    .then((data) => {
      getFilmsFromPlaylist(data, idPlaylist);
    })
    .catch((error) => {
      console.log(error);
    });
}

//FONCTION POUR AVOIR LES INFOS D'UNE PLAYLIST PRECISE
function getInfosFromPlaylist(playlist) {
  const list = document.getElementById("infoPlaylist");
  list.style.display = "initial";
  document.getElementById("listPlaylist").style.display = "none";
  document.getElementById("listFilm").style.display = "none";
  document.getElementById("listAvis").style.display = "none";
  document.getElementById("listHashtag").style.display = "none";
  document.getElementById("listRecherche").style.display = "none";
  document.getElementById("formFilminPlaylist").style.display = "none";
  document.getElementById("formRemplirPlaylist").style.display = "none";
  document.getElementById("formGenre").style.display = "none";
  document.getElementById("formAvis").style.display = "none";
  document.getElementById("formHashtag").style.display = "none";

  var content =
    "<table><tr><td>Titre</td><td>Auteur</td><td>Date</td><td>Description</td></tr>";

  content +=
    "<tr><td>" +
    playlist.titre +
    "</td><td>" +
    playlist.auteur +
    "</td><td>" +
    playlist.date +
    "</td><td>" +
    playlist.description +
    "</tr></td>";
  if (playlist.MOIS == false) {
    content +=
      "<button class='removePlaylist-button' onclick='removePlaylist(\"" +
      playlist.idPlaylist +
      "\")'>Supprimer cette playlist</button>";
  }
  content += "</table></br>";
  list.innerHTML = content;
}

//FONCTION POUR AVOIR LES FILMS D'UNE PLAYLIST PRECISE
function getFilmsFromPlaylist(films, idPlaylist) {
  const list = document.getElementById("listFilms");
  list.style.display = "flex";
  document.getElementById("listFilm").style.display = "none";
  document.getElementById("listPlaylist").style.display = "none";
  document.getElementById("listAvis").style.display = "none";
  document.getElementById("listHashtag").style.display = "none";
  document.getElementById("listRecherche").style.display = "none";
  document.getElementById("formFilminPlaylist").style.display = "none";
  document.getElementById("listGenre").style.display = "none";
  document.getElementById("formGenre").style.display = "none";
  document.getElementById("formAvis").style.display = "none";
  document.getElementById("formHashtag").style.display = "none";
  verifieplaylistdumois(idPlaylist);

  var content = "";
  films.forEach(function (film) {
    content +=
      "<div class='film-card'><p>" +
      film.titre +
      "</p><img src='" +
      film.lienAffiche +
      "'/>" +
      "<button class='voirPlaylist-button' onclick='afficherFilm(\"" +
      film.idFilm +
      "\")'>Plus d'infos sur le film</button>" +
      "<button class='removefilmfromplaylist-button' onclick='removefilmfromplaylist(\"" +
      film.idFilm +
      '","' +
      idPlaylist +
      "\")'>Retirer de la playlist</button>";
    if (mois) {
      content +=
        "<p>Nombre de vote :</p>" +
        "<div class='lignevote'><button class='addDownvote-button' onclick='addDownvote(\"" +
        film.idFilm +
        '","' +
        idPlaylist +
        "\")'>-1 </button>" +
        film.NoteFilm +
        "<button class='addUpvote-button' onclick='addUpvote(\"" +
        film.idFilm +
        '","' +
        idPlaylist +
        "\")'>+1</button></div>";
    }
    content += "</div >";
  });

  content +=
    "<button class='formulaireajoutfilm-button' onclick='afficherformulaireFilm(\"" +
    idPlaylist +
    "\")'>Ajouter un film dans la playlist</button></div>";
  list.innerHTML = content;
}

//SUPPRIMER UN FILM DANS UNE PLAYLIST
function removefilmfromplaylist(idFilm, idPlaylist) {
  fetch("./router.php/filmsInPlaylist/" + idFilm + "/" + idPlaylist, {
    method: "DELETE",
  })
    .then((response) => response.json())
    .then((data) => {
      getFilmsFromPlaylist(data, idPlaylist);
    })
    .catch((error) => {
      console.log(error);
    });
}

//AFFICHER LE FORMULAIRE D'AJOUT D'UN PLAYLIST
function showFormPlaylist() {
  document.getElementById("formFilm").style.display = "none";
  document.getElementById("formPlaylist").style.display = "initial";
}

//AJOUTER UNE PLAYLIST
document.getElementById("addingPlaylist").onclick = (event) => {
  event.preventDefault();
  const form = {};
  form.titre = document.getElementById("input-titre-playlist").value;
  form.desc = document.getElementById("input-desc").value;
  form.auteur = document.getElementById("input-auteur").value;
  form.url = document.getElementById("input-url").value;

  fetch("./router.php/playlists/", {
    method: "POST",
    body: JSON.stringify(form),
  })
    .then((response) => response.json())
    .then((data) => {
      getAllPlaylists(data);
    })
    .catch((error) => {
      console.log(error);
    });
};

//ENLEVER LE FORMULAIRE PLAYLIST
document.getElementById("cancelPlaylist").onclick = (event) => {
  event.preventDefault();
  document.getElementById("formPlaylist").style.display = "none";
};

//VIDER UNE PLAYLIST
function viderPlaylist(films, idPlaylist) {
  films.forEach(function (film) {
    removefilmfromplaylist(film.idFilm, idPlaylist);
  });
}

//SUPPRIMER UNE PLAYLIST
function removePlaylist(idPlaylist) {
  fetch("./router.php/filmsInPlaylist/" + idPlaylist, { method: "GET" })
    .then((response) => response.json())
    .then((data) => {
      viderPlaylist(data, idPlaylist);
    })
    .catch((error) => {
      console.log(error);
    });

  fetch("./router.php/playlist/" + idPlaylist, { method: "DELETE" })
    .then((response) => response.json())
    .then((data) => {
      afficherAllPlaylists(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

//VERIFIER SI C'EST UNE PLAYLIST DU MOIS
function verifieplaylistdumois(idPlaylist) {
  fetch("./router.php/playlist/" + idPlaylist, {
    method: "GET",
  })
    .then((response) => response.json())
    .then((data) => {
      mois = data.MOIS;
    })
    .catch((error) => {
      console.log(error);
    });
}
////////////////-----------FILM---------------//////////////

//REMPLIR LE FORMULAIRE AVEC TOUTES LES FILMS
function remplirformulairefilm(films) {
  const formulaire = document.getElementById("films_dispo-select");
  var content = "";

  films.forEach(function (film) {
    content += "<option value=" + film.idFilm + ">" + film.titre + "</option>";
  });
  formulaire.innerHTML = content;
}

//AFFICHER TOUS LES FILMS
function afficherAllFilms() {
  fetch("./router.php/films", { method: "GET" })
    .then((response) => response.json())
    .then((data) => {
      remplirformulairefilm(data);
      displayFilms(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

function displayFilms(films) {
  const list = document.getElementById("listFilms");
  list.style.display = "flex";
  document.getElementById("listFilm").style.display = "none";
  document.getElementById("listPlaylist").style.display = "none";
  document.getElementById("infoPlaylist").style.display = "none";
  document.getElementById("listAvis").style.display = "none";
  document.getElementById("listRecherche").style.display = "none";
  document.getElementById("listHashtag").style.display = "none";
  document.getElementById("formFilminPlaylist").style.display = "none";
  document.getElementById("formRemplirPlaylist").style.display = "none";
  document.getElementById("listGenre").style.display = "none";
  document.getElementById("formGenre").style.display = "none";
  document.getElementById("formAvis").style.display = "none";
  document.getElementById("formHashtag").style.display = "none";
  var content = "";

  films.forEach(function (film) {
    content +=
      "<div class='film-card'><p>" +
      film.titre +
      "</p><img src='" +
      film.lienAffiche +
      "'/>" +
      "<button class='voirPlaylist-button' onclick='afficherFilm(\"" +
      film.idFilm +
      "\")'>Plus d'infos sur le film</button>" +
      "<button class='formulaireajout-button' onclick='afficherformulaire(\"" +
      film.idFilm +
      "\")'>Ajouter le film dans une playlist</button></div>";
  });
  list.innerHTML = content;
}

//SUPPRIMER UN FILM
function remove(id) {
  fetch("./router.php/film/" + id, { method: "DELETE" })
    .then((response) => response.json())
    .then((data) => {
      displayFilms(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

//OBTENIR INFO FILM
function afficherFilm(id) {
  fetch("./router.php/film/" + id, { method: "GET" })
    .then((response) => response.json())
    .then((data) => {
      displayFilm(data);
    })
    .catch((error) => {
      console.log(error);
    });
  fetch("./router.php/avis/" + id, { method: "GET" })
    .then((response) => response.json())
    .then((data) => {
      displayAvis(data);
    })
    .catch((error) => {
      console.log(error);
    });
  fetch("./router.php/hashtag/" + id, { method: "GET" })
    .then((response) => response.json())
    .then((data) => {
      displayHashtag(data);
    })
    .catch((error) => {
      console.log(error);
    });
  fetch("./router.php/genrefilm/" + id, { method: "GET" })
    .then((response) => response.json())
    .then((data) => {
      displayGenre(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

//AFFICHER INFOS FILM
function displayFilm(film) {
  const listFilm = document.getElementById("listFilm");
  listFilm.style.display = "initial";
  document.getElementById("listPlaylist").style.display = "none";
  document.getElementById("listFilms").style.display = "none";
  document.getElementById("infoPlaylist").style.display = "none";
  document.getElementById("listRecherche").style.display = "none";
  document.getElementById("formFilminPlaylist").style.display = "none";
  document.getElementById("formRemplirPlaylist").style.display = "none";
  document.getElementById("formHashtag").style.display = "none";
  var content = "";
  content +=
    "<p>Titre: " +
    film.titre +
    "</p><p>Réalisateur: " +
    film.realisateur +
    "</p><p>Durée: " +
    film.duree +
    "</p><p>Nationalité: " +
    film.nationalite +
    "</p><p>Année: " +
    film.annee +
    "</p><img src='" +
    film.lienAffiche +
    "'/><div id='sous-menu'><button onclick = 'showFormHashtag()'>Ajouter un hashtag</button>" +
    "<button class='formulairegenre-button' onclick='showFormGenre(\"" +
    film.idFilm +
    "\")'>Ajouter genre</button><button onclick='showFormAvis()'>Ajouter un avis</button></div><button class='removeFilm-button' onclick='remove(\"" +
    film.idFilm +
    "\")'>Supprimer ce film</button>";

  idFilmFK = film.idFilm;
  listFilm.innerHTML = content;
}

//AFFICHER FORMULAIRE AJOUT FILM
function showFormFilm() {
  document.getElementById("formPlaylist").style.display = "none";
  document.getElementById("formFilm").style.display = "initial";
}

//AJOUTER UN FILM
document.getElementById("addingFilm").onclick = (event) => {
  event.preventDefault();
  document.getElementById("formFilm").style.display = "none";

  const form = {};
  form.titre = document.getElementById("input-titre").value;
  form.realisateur = document.getElementById("input-realisateur").value;
  form.duree = document.getElementById("input-duree").value;
  form.nationalite = document.getElementById("input-nationalite").value;
  form.annee = document.getElementById("input-annee").value;
  form.lienAffiche = document.getElementById("input-lienAffiche").value;

  fetch("./router.php/film", { method: "POST", body: JSON.stringify(form) })
    .then((response) => response.json())
    .then((data) => {
      displayFilms(data);
    })
    .catch((error) => {
      console.log(error);
    });
};

//AJOUTER LE FILM DANS UNE PLAYLIST
function ajouterFilmdansPlaylist(idFilm) {
  idPlaylist = document.getElementById("playlists_dispo-select").value;
  const form = {};
  form.idFilm = idFilm;
  form.idPlaylist = idPlaylist;
  fetch("./router.php/filmsInPlaylist/" + idFilm + "/" + idPlaylist, {
    method: "POST",
    body: JSON.stringify(form),
  })
    .then((response) => response.json())
    /*.then((data) => {
      displayFilms(data);
    })*/
    .catch((error) => {
      console.log(error);
    });
}

//REMPLIR LA PLAYLIST AVEC UN FILM
function remplirPlaylistavecFilm(idPlaylist) {
  idFilm = document.getElementById("films_dispo-select").value;
  const form = {};
  form.idFilm = idFilm;
  form.idPlaylist = idPlaylist;
  fetch("./router.php/filmsInPlaylist/" + idFilm + "/" + idPlaylist, {
    method: "POST",
    body: JSON.stringify(form),
  })
    .then((response) => response.json())
    .then((data) => {
      displayFilms(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

//CONFIRMER LE CHOIX DE LA PLAYLIST DE DESTINATION
function validerFilmdansPlaylist(idFilm) {
  document.getElementById("validerchoixplaylist").onclick = (event) => {
    event.preventDefault();
    ajouterFilmdansPlaylist(idFilm);
  };
}

function validerFilmpourPlaylist(idPlaylist) {
  document.getElementById("validerchoixfilm").onclick = (event) => {
    event.preventDefault();
    remplirPlaylistavecFilm(idPlaylist);
  };
}

//AFFICHER LE FORMULAIRE POUR CHOISIR LA PLAYLIST OU METTRE LE FILM
function afficherformulaire(idFilm) {
  document.getElementById("formFilminPlaylist").style.display = "initial";
  validerFilmdansPlaylist(idFilm);
}

function afficherformulaireFilm(idPlaylist) {
  document.getElementById("formRemplirPlaylist").style.display = "initial";
  validerFilmpourPlaylist(idPlaylist);
}

//ENLEVER LE FORMULAIRE POUR CHOISIR UNE PLAYLIST
document.getElementById("cancelFormuPlaylist").onclick = (event) => {
  event.preventDefault();
  document.getElementById("formFilminPlaylist").style.display = "none";
};

//ENLEVER LE FORMULAIRE POUR CHOISIR UN FILM
document.getElementById("cancelFormuFilm").onclick = (event) => {
  event.preventDefault();
  document.getElementById("formRemplirPlaylist").style.display = "none";
};

//ENLEVER LE FORMULAIRE FILM
document.getElementById("cancelFilm").onclick = (event) => {
  event.preventDefault();
  document.getElementById("formFilm").style.display = "none";
};

////////////////-----------AVIS---------------//////////////

//AFFICHER LE FORMULAIRE D'AJOUT D'UN FILM
function showFormAvis() {
  document.getElementById("formAvis").style.display = "initial";
}

//AJOUTER UN AVIS
document.getElementById("addingAvis").onclick = (event) => {
  event.preventDefault();
  document.getElementById("formAvis").style.display = "none";
  const form = {};
  form.note = document.getElementById("input-note").value;
  form.commentaire = document.getElementById("input-commentaire").value;
  form.pseudo = document.getElementById("input-pseudo").value;
  form.idFilmFK = idFilmFK;

  fetch("./router.php/avis", {
    method: "POST",
    body: JSON.stringify(form),
  })
    .then((response) => response.json())
    .then((data) => {
      displayAvis(data);
    })
    .catch((error) => {
      console.log(error);
    });
};

//AFFICHER TOUS LES AVIS
function displayAvis(avis) {
  const list = document.getElementById("listAvis");
  list.style.display = "initial";
  var content =
    "<table><tr><td>Note</td><td>Commentaire</td><td>Pseudo</td></tr>";

  avis.forEach(function (avi) {
    content +=
      "<tr><td>" +
      avi.note +
      "</td><td>" +
      avi.commentaire +
      "</td><td>" +
      avi.pseudo +
      "</td><td><button onclick='supprAvis(\"" +
      avi.idAvis +
      '","' +
      avi.idFilmFK +
      "\")'>Remove</button></td></tr>";
  });
  content += "</table>";
  list.innerHTML = content;
}

//SUPPRIMER UN AVIS
function supprAvis(idAvis, idFilm) {
  fetch("./router.php/avi/" + idAvis + "/" + idFilm, { method: "DELETE" })
    .then((response) => response.json())
    .then((data) => {
      displayAvis(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

//ENLEVER LE FORMULAIRE AVIS
document.getElementById("cancelAvis").onclick = (event) => {
  event.preventDefault();
  document.getElementById("formAvis").style.display = "none";
};

////////////////------------HASHTAG--------------//////////////

//AFFICHER FORMULAIRE AJOUT HASHTAG
function showFormHashtag() {
  document.getElementById("formHashtag").style.display = "initial";
}

//ENLEVER LE FORMULAIRE HASHTAG
document.getElementById("cancelHashtag").onclick = (event) => {
  event.preventDefault();
  document.getElementById("formHashtag").style.display = "none";
};

//AJOUTER UN HASHTAG
function addinghashtag() {
  const form = {};
  form.idFilm = idFilmFK;
  form.nom = document.getElementById("input-hashtag").value;

  fetch("./router.php/hashtag", {
    method: "POST",
    body: JSON.stringify(form),
  })
    .then((response) => response.json())
    .then((data) => {
      displayHashtag(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

//AFFICHER TOUS LES HASHTAG
function displayHashtag(hashtags) {
  const list = document.getElementById("listHashtag");
  list.style.display = "block";

  var content = "";
  hashtags.forEach(function (hashtag) {
    content +=
      "<p class='hashtag' onclick='voirFilmsHashtag(\"" +
      hashtag.idHashtag +
      "\")'>" +
      hashtag.nom +
      "</p><button onclick='supprHashtag(\"" +
      hashtag.idHashtag +
      "\")'>X</button>";
  });

  list.innerHTML = content;
}

//AFFICHER LES FILMS ASSOCIES A UN HASHTAG
function voirFilmsHashtag(id) {
  fetch("./router.php/hashtags/" + id, { method: "GET" })
    .then((response) => response.json())
    .then((data) => {
      displayFilms(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

//SUPPRIMER UN HASHTAG
function supprHashtag(idHashtag) {
  fetch("./router.php/hashtag/" + idFilmFK + "/" + idHashtag, {
    method: "DELETE",
  })
    .then((response) => response.json())
    .then((data) => {
      displayHashtag(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

////////////////////----BARRE DE RECHERCHE----//////////////////////////

// on peut retourner deux types de résultats : des films ou des playlists
document.getElementById("buttonRecherche").onclick = (event) => {
  event.preventDefault();
  const search = document.getElementById("search").value;
  const recherche = document.getElementById("listRecherche");
  recherche.innerHTML = "";

  fetch("./router.php/recherchefilm/" + search, {
    method: "GET",
  })
    .then((response) => response.json())
    .then((data) => {
      displayRechercheFilm(data);
    })
    .catch((error) => {
      console.log(error);
    });

  fetch("./router.php/rechercheplaylist/" + search, {
    method: "GET",
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      displayRecherchePlaylist(data);
    })
    .catch((error) => {
      console.log(error);
    });
};

//AFFICHER RESULTATS RECHERCHE FILMS
function displayRechercheFilm(films) {
  const recherche = document.getElementById("listRecherche");
  recherche.style.display = "flex";
  document.getElementById("listFilm").style.display = "none";
  document.getElementById("listFilms").style.display = "none";
  document.getElementById("listPlaylist").style.display = "none";
  document.getElementById("listHashtag").style.display = "none";
  document.getElementById("formFilminPlaylist").style.display = "none";
  document.getElementById("formRemplirPlaylist").style.display = "none";
  document.getElementById("formGenre").style.display = "none";
  document.getElementById("formAvis").style.display = "none";
  document.getElementById("listAvis").style.display = "none";
  document.getElementById("formHashtag").style.display = "none";

  var content = "";
  films.forEach(function (film) {
    content +=
      "<div class='film-card'><p>" +
      film.titre +
      "</p><img src='" +
      film.lienAffiche +
      "'/>" +
      "<button class='voirPlaylist-button' onclick='afficherFilm(\"" +
      film.idFilm +
      "\")'>Plus d'infos sur le film</button></div>";
  });

  recherche.innerHTML += content;
}

//AFFICHER RESULTATS RECHERCHE PLAYLISTS
function displayRecherchePlaylist(playlists) {
  const recherche = document.getElementById("listRecherche");
  recherche.style.display = "flex";
  document.getElementById("listFilm").style.display = "none";
  document.getElementById("listFilms").style.display = "none";
  document.getElementById("listPlaylist").style.display = "none";
  document.getElementById("listHashtag").style.display = "none";
  document.getElementById("formFilminPlaylist").style.display = "none";
  document.getElementById("formRemplirPlaylist").style.display = "none";
  document.getElementById("formGenre").style.display = "none";
  document.getElementById("formAvis").style.display = "none";
  document.getElementById("listAvis").style.display = "none";
  document.getElementById("formHashtag").style.display = "none";

  var content = "";
  playlists.forEach(function (playlist) {
    content +=
      "<div class='playlist-card'><p>" +
      playlist.titre +
      "</p><img src='" +
      playlist.lienPhoto +
      "'/>" +
      "<button class='voirPlaylist-button' onclick='voirPlaylist(\"" +
      playlist.idPlaylist +
      "\")'>Plus d'infos sur la playlist</button></div>";
  });

  recherche.innerHTML += content;
}

////////////////------------VOTE--------------//////////////

//METTRE UN UPVOTE
function addUpvote(idFilm, idPlaylist) {
  fetch("./router.php/upvote/" + idFilm + "/" + idPlaylist, {
    method: "UPDATE",
  })
    .then((response) => response.json())
    .then((data) => {
      getFilmsFromPlaylist(data, idPlaylist);
    })
    .catch((error) => {
      console.log(error);
    });
}

//METTRE UN DOWNVOTE
function addDownvote(idFilm, idPlaylist) {
  fetch("./router.php/downvote/" + idFilm + "/" + idPlaylist, {
    method: "UPDATE",
  })
    .then((response) => response.json())
    .then((data) => {
      getFilmsFromPlaylist(data, idPlaylist);
    })
    .catch((error) => {
      console.log(error);
    });
}

////////////////////----GENRE----//////////////////////////

//AFFICHER LE FORMULAIRE POUR CHOISIR LA PLAYLIST OU METTRE LE FILM
function showFormGenre(idFilm) {
  document.getElementById("formGenre").style.display = "initial";
}

//REMPLIR FORMULAIRE GENRE
function remplirformulairegenre(genres) {
  const formulaire = document.getElementById("genres_dispo-select");
  var content = "";

  genres.forEach(function (genre) {
    content += "<option value=" + genre.idGenre + ">" + genre.nom + "</option>";
  });
  formulaire.innerHTML = content;
}

//AFFICHER TOUS LES GENRES
function getAllGenres() {
  fetch("./router.php/genre", { method: "GET" })
    .then((response) => response.json())
    .then((data) => {
      remplirformulairegenre(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

//METTRE UN GENRE A UN FILM
function mettreGenreFilm(idFilm) {
  idGenre = document.getElementById("genres_dispo-select").value;
  fetch("./router.php/genrefilm/" + idFilm + "/" + idGenre, {
    method: "POST",
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      displayGenre(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

//CONFIRMER LE CHOIX DU GENRE
document.getElementById("validerchoixGenre").onclick = (event) => {
  event.preventDefault();
  mettreGenreFilm(idFilmFK);
};

//ENLEVER LE FORMULAIRE POUR CHOIX GENRE
document.getElementById("cancelGenre").onclick = (event) => {
  event.preventDefault();
  document.getElementById("formGenre").style.display = "none";
};

//AFFICHER TOUS LES GENRES
function displayGenre(genres) {
  const list = document.getElementById("listGenre");
  list.style.display = "block";

  var content = "";
  genres.forEach(function (genre) {
    content +=
      "<p class='genre' onclick='voirFilmsGenre(\"" +
      genre.idGenre +
      "\")'>" +
      genre.nom +
      "</p><button onclick='supprGenre(\"" +
      genre.idGenre +
      "\")'>X</button>";
  });

  list.innerHTML = content;
}

//
function voirFilmsGenre(id) {
  fetch("./router.php/filmgenre/" + id, { method: "GET" })
    .then((response) => response.json())
    .then((data) => {
      displayFilms(data);
    })
    .catch((error) => {
      console.log(error);
    });
}

//SUPPRIMER UN GENRE
function supprGenre(idGenre) {
  fetch("./router.php/genrefilm/" + idFilmFK + "/" + idGenre, {
    method: "DELETE",
  })
    .then((response) => response.json())
    .then((data) => {
      displayGenre(data);
    })
    .catch((error) => {
      console.log(error);
    });
}
