CREATE SCHEMA IF NOT EXISTS movimac;
USE movimac;

CREATE TABLE Film (
    idFilm int(4) NOT NULL AUTO_INCREMENT, 
    titre varchar(180) NOT NULL, 
    realisateur varchar(50) NOT NULL, 
    duree time NOT NULL,
    nationalite varchar(30), 
    annee year NOT NULL, 
    lienAffiche varchar(400),
    PRIMARY KEY (idFilm)
);

CREATE TABLE Avis (
    idAvis int(4) NOT NULL AUTO_INCREMENT, 
    note int(1), 
    commentaire varchar(300),
    pseudo varchar(50) NOT NULL,
    idFilmFK int(4) NOT NULL, 
    PRIMARY KEY (idAvis),
    FOREIGN KEY (idFilmFK) REFERENCES Film(idFilm)
);

CREATE TABLE Playlist(
    idPlaylist int(4) NOT NULL AUTO_INCREMENT,
    titre varchar(30) NOT NULL,
    description varchar(100),
    date date NOT NULL,
    auteur varchar(50) NOT NULL,
    lienPhoto varchar(400),
    PRIMARY KEY (idPlaylist)
);

CREATE TABLE Hashtag (
    idHashtag int(4) NOT NULL,
    nom varchar(30),
    PRIMARY KEY (idHashtag)
);

CREATE TABLE Genre (
    idGenre int(4) NOT NULL,
    nom varchar(30),
    PRIMARY KEY (idGenre)
);

CREATE TABLE FilmPlaylistRelation(
    idFilm int(4) NOT NULL,
    idPlaylist int(4) NOT NULL,
    PRIMARY KEY (idFilm, idPlaylist),
    FOREIGN KEY (idFilm) REFERENCES Film(idFilm), 
    FOREIGN KEY (idPlaylist) REFERENCES Playlist(idPlaylist)
);

CREATE TABLE FilmHashtagRelation(
    idFilm int(4) NOT NULL,
    idHashtag int(4) NOT NULL,
    PRIMARY KEY (idFilm, idHashtag),
    FOREIGN KEY (idFilm) REFERENCES Film(idFilm), 
    FOREIGN KEY (idHashtag) REFERENCES Hashtag(idHashtag)
);

CREATE TABLE FilmGenreRelation(
    idFilm int(4) NOT NULL,
    idGenre int(4) NOT NULL,
    PRIMARY KEY (idFilm, idGenre),
    FOREIGN KEY (idFilm) REFERENCES Film(idFilm), 
    FOREIGN KEY (idGenre) REFERENCES Genre(idGenre)
);