<?php
	require_once('model.php');

////////////////////----PLAYLIST----//////////////////////////

	function getAPlaylist($titre) {
		return json_encode(getPlaylist($titre));
	}

	function getPlaylistsAsTable() {
		return json_encode(getAllPlaylists());
	}

	function getFilmsFromPlaylistAsTable($titre) {
		return json_encode(getFilmsFromPlaylist($titre));
	}

	function deleteAFilmFromPlaylist($idFilm, $idPlaylist) {
		return json_encode(deleteFilmFromPlaylist($idFilm, $idPlaylist));
	}

	function addAPlaylist($form) {
		$playlist = json_decode($form, true);
		return json_encode(addPlaylist($playlist['titre'], $playlist['desc'], $playlist['auteur'], $playlist['url']));
	}

	function deleteAPlaylist($id) {
		return json_encode(deletePlaylist($id));
	}

////////////////////----FILM----//////////////////////////
	
	function getFilmsAsTable() {
		return json_encode(getAllFilm());
	}

	function getAFilm($id) {
		return json_encode(getFilm($id));
	}

	function addAFilm($form) {
		//we want the data as an associative array
		$film = json_decode($form, true);
		return json_encode(addFilm($film['titre'], $film['realisateur'], $film['duree'], $film['nationalite'], $film['annee'], $film['lienAffiche']));
	}

	function deleteAFilm($id) {
		return json_encode(deleteFilm($id));
	}
	
	function addaFilmintoPlaylist($idFilm,$idPlaylist){
        return json_encode(filmdansPlaylist($idFilm, $idPlaylist));
    }

////////////////////----AVIS----//////////////////////////

	function addaAvis($form){
		$avis = json_decode($form, true);
		return json_encode(addAvis($avis['note'], $avis['commentaire'], $avis['pseudo'], $avis['idFilmFK']));
	}
	
	function getAvisAsTable($id) {
		return json_encode(getAllAvis($id));
	}

	function getaAvis($idFilm,$idAvis){
		return json_encode(getAvis($idFilm,$idAvis));
	}

	function removeaAvis($idAvis, $idFilm){
		return json_encode(removeAvis($idAvis, $idFilm));
	}

////////////////////----HASHTAG----//////////////////////////

	function getallFilmsFromHashtagAsTable($id) {
		return json_encode(getAllFilmsFromHashtag($id));
	}

	function getAllHashtagsFromFilmAsTable($id){
		return json_encode(getAllHashtagsFromFilm($id));
	}

	function addAHashtag($form) {
		$hashtag = json_decode($form, true);
		return json_encode(addHashtag($hashtag['idFilm'], $hashtag['nom']));
	}

	function deleteAHashtag($idFilm, $idHashtag) {
		return json_encode(deleteHashtag($idFilm, $idHashtag));
	}

////////////////////----BARRE DE RECHERCHE----//////////////////////////

	function recherchesFilms($titre){
		return json_encode(rechercheFilm($titre));
	}

	function recherchesPlaylists($titre){
		return json_encode(recherchePlaylist($titre));
	}
////////////////////----VOTE----//////////////////////////

function putAnUpVote($idFilm, $idPlaylist){
	return json_encode(addUpVote($idFilm, $idPlaylist));
}

function putADownVote($idFilm, $idPlaylist){
	return json_encode(addDownVote($idFilm, $idPlaylist));
}

////////////////////----GENRE----//////////////////////////

function getAllGenresFromFilmAsTable($id){
	return json_encode(getAllGenresFromFilm($id));
}

function addAGenre($idFilm, $idGenre) {
	return json_encode(addGenre($idFilm, $idGenre));
}

function deleteAGenre($idFilm, $idGenre) {
	return json_encode(deleteGenre($idFilm, $idGenre));
}

function getGenresAsTable() {
	return json_encode(getAllGenres());
}
function getFilmsFromGenreAsTable($id){
	return json_encode(getAllFilmsFromGenre($id));
}